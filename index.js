const fs = require('fs')
const path = require('path')
const index = []

fs.readdirSync('.').forEach(dir => {
  if (!dir.startsWith('.') && dir !== 'icons' && fs.lstatSync(dir).isDirectory()) {
    fs.readdirSync(dir).forEach(file => {
      if (file.endsWith('.json') && !file.startsWith('index')) {
        // extract id (= filename without ending)
        const id = file.substring(0, file.length - 5)
        // read json file
        const json = fs.readFileSync(path.join(dir, file))
        const item = JSON.parse(json)
        let icon = ''
        // try to receive icon
        if (item.icon && item.icon.startsWith('data:image/')) {
          const parts = item.icon.trim().split(';')
          let iconType
          if (parts[0].indexOf('svg') > 0) iconType = 'svg'
          else if (parts[0].indexOf('png') > 0) iconType = 'png'
          else if (parts[0].indexOf('jpeg') > 0) iconType = 'jpg'
          else if (parts[0].indexOf('webp') > 0) iconType = 'webp'
          const base64 = parts[1].substring(6)
          const buffer = Buffer.from(base64, 'base64')
          if (iconType) {
            icon = `${dir}-${id}.${iconType}`
            fs.writeFileSync(`./icons/${icon}`, buffer)
          }
          else {
            console.log(`Unknown icon type for ${dir}/${file}`)
          }
        } else {
          console.log(`Missing icon for ${dir}/${file}`)
        }
        index.push({
          id,
          type: dir,
          title: item.title ? item.title : '',
          file,
          subject: item.subject ? item.subject : '',
          desc: item.desc ? item.desc : '',
          locale: item.locale ? item.locale : '',
          author: item.author ? item.author : '',
          license: item.license ? item.license : '',
          icon
        })
      }
    })
  }
})

fs.writeFileSync('index.json', JSON.stringify(index, null, "  "))
