# License of external images

## Plot

- plot-radon.svg: [MaxxL](https://commons.wikimedia.org/wiki/User:MaxxL), https://de.wikipedia.org/wiki/Radon#/media/Datei:ISO_7010_W003.svg, public domain

## Poll

- poll-beispiel.svg: https://commons.wikimedia.org/wiki/File:Checklist-icon.svg, CC-0
- poll-unterricht.svg: [Offnfopt](https://commons.wikimedia.org/wiki/User:Offnfopt), https://commons.wikimedia.org/wiki/File:Assassination_Classroom_-_Koro-sensei_smiling_head.svg, public domain

## Quiz

- quiz-password.jpg: [Vijay Verma](https://3dicons.co/), https://commons.wikimedia.org/wiki/File:Key-dynamic-gradient.png, CC-0
